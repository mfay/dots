﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConvexHullScan
{
    public partial class Form1 : Form
    {
        List<Point> redDots = new List<Point>();
        List<Point> blueDots = new List<Point>();
        public static readonly int dotsize = 3;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            Point pt = new Point(e.X, e.Y);
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                redDots.Add(pt);
            }
            else
            {
                blueDots.Add(pt);
            }
            this.Text = string.Format("x: {0} y: {1}", e.X, e.Y);
            this.Refresh();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            foreach (Point pt in redDots)
            {
                g.DrawEllipse(Pens.Red, pt.X, pt.Y, dotsize, dotsize);
            }
            foreach (Point pt in blueDots)
            {
                g.DrawEllipse(Pens.Blue, pt.X, pt.Y, dotsize, dotsize);
            }
        }


    }
}
